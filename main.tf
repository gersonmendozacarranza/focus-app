variable "do_token" {}

provider "digitalocean" {
    token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "cluster" {
    name    = "focus-cluster"
    region  = "nyc1"
    version = "1.23.10-do.0"
    node_pool {
        name       = "focus-pool"
        size       = "s-1vcpu-2gb"
        node_count = 2
    }
}

terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}
