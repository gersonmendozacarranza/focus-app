terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/38235914/terraform/state/focus-app"
    lock_address = "https://gitlab.com/api/v4/projects/38235914/terraform/state/focus-app/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/38235914/terraform/state/focus-app/lock"
  }
}